import en_US from './en_US';
import fr_FR from './fr_FR';

export default {
    en_US,
    fr_FR
}